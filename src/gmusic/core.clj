(ns gmusic.core
  (:use seesaw.core seesaw.mig seesaw.table)
  (:gen-class))

(def google_client (.create (com.faceture.google.play.PlayClientBuilder.)))

(def login
  (frame
    :title "Login"
    :on-close :exit
    :content (mig-panel
      :constraints [
        "wrap 2"
        "[shrink 0]20px[200, grow, fill]"
        "[shrink 0]5px[]"]
      :items [
        ["email:"]    [(text :id :email)]
        ["password:"] [(text :id :password)]
        [:fill-h]     [(button :id :login_button :text "Login")]])))

(defn library
  [session]
  (frame
    :title "Library"
    :on-close :exit
    :content (let
      [search_input (text :id :search)
       results_table (table :model [:columns [:artist :album :title]
                                    :rows    []])]
      (do
        (listen results_table
                  :selection (fn [e] (alert
                                       (.getPlayURI google_client
                                          (:id (value-at results_table (selection results_table)))
                                          session))))
        (mig-panel
        :constraints [
          "wrap 2"
          "[shrink 0]20px[200, grow, fill]"
          "[shrink 0]5px[]"]
        :items [
          ["search:"] [search_input]
          [:fill-h]   [(button
                          :id :search_button
                          :text "Search"
                          :listen
                            [:action (fn [e] (let
                              [results (.search google_client (value search_input) session)]
                              (config! results_table
                                         :model [:columns [:artist :album :title]
                                                 :rows (map bean (.getSongs results))])))])]
          [results_table "span"]])))))

(listen
  (select login [:#login_button])
  :action (fn [e] (let
                    [login_data     (value login)
                     login_response (.login google_client (:email login_data) (:password login_data))
                     login_result   (.getLoginResult login_response)]

                    (if (= login_result com.faceture.google.play.LoginResult/SUCCESS)
                        (do (dispose! login)
                            (-> (library (.getPlaySession login_response)) pack! show!))
                        (alert login_result)))))

(defn -main [& args]
  (native!)
  (invoke-later
    (-> login
     pack!
     show!)))
