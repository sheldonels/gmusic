(defproject gmusic "0.0.1"
  :description "Google Music Desktop App"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [seesaw "1.4.3"]

                 [org.apache.httpcomponents/httpclient "4.1.3"]
                 [org.apache.httpcomponents/httpcore "4.1.4"]
                 [commons-logging/commons-logging "1.1.1"]
                 [commons-codec/commons-codec "1.4"]
                 [org.apache.httpcomponents/httpmime "4.1.3"]
                 [com.google.code.gson/gson "2.1"]]
  :java-source-paths ["src/main/java"]
  :main gmusic.core
  :aot [gmusic.core])
